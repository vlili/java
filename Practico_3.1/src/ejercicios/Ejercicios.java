
package ejercicios;

import java.util.Scanner;

public class Ejercicios {


public static void main(String[] args) {
//5. Desarrollar un programa Java que muestre por pantalla los números primos del 1 al
//1000 y todos los años bisiestos entre el año 2000 y el 3000.
int contdiv;  
System.out.println("ejercico 5 :");
System.out.println("Numeros primos del 1 al 1000 :");
for(int i=1;i <(1000-1);i++){
contdiv = 0;    
for(int e=1; e <= i; e++ ) {
    
if(i%e == 0){
contdiv ++;
}
}
if(contdiv == 2){
System.out.println(i);
}  
}
System.out.println("Los años bisiestos desde el año 2000 hasta el año 3000 ");

for(int s=2000; s < 3000 ;s=s+4){
   
System.out.println(s);
}
 
    
//6. Desarrollar un programa Java que calcule el factorial de un número entero.
int numero =0;
int resultado=1;
Scanner read = new Scanner(System.in);
System.out.println("ejercico 6 :");
System.out.println("Ingrese un numero entero para conocer su factorial :");
numero = read.nextInt();

for (int v=1; v <= numero; v++){
resultado *=v;
}
 System.out.println(numero+"! ="+resultado);
 
 //7. Desarrollar un programa Java que sume los números del 1 al 100 (ambos inclusive).
  resultado=0;
 System.out.println("Ejercicio 7: ");
 for (int b=1; b<=100;b++){
  resultado +=b;  
     
 }
 System.out.println("La suma de 1 al 100 es : "+resultado);
 
// 8. Crea un array o arreglo unidimensional con un tamaño de 5, asignar los valores
//numéricos manualmente (los que tu quieras) y mostrarlos por pantalla utilizando las
//estructuras cíclicas vistas en clase.
int[] ve= {12,13,15,7,8,9}; 

System.out.println("Ejercico 8 , mostrar los datos del vector estatico .");
for(int v=0; v < ve.length; v++){
 System.out.println(ve[v]);
}


//9. ​ Modifica el ejercicio anterior para que insertes los valores numéricos mediante un bucle
//con consola y los muestre por pantalla.
int[] vec= new int[5];

System.out.println("Ejercico 9, Ingrese 5 valores enteros :");

for (int c=0; c < vec.length;c++ ){
      
Scanner sc = new Scanner(System.in) ;

vec[c]=sc.nextInt();

}
System.out.println("Mostrar los datos ingresados :");
for(int c =0; c < vec.length;c++) {
 
 System.out.println(vec[c]);
}


//10. ​ Crea un array o arreglo unidimensional donde tu le indiques el tamaño por teclado y
//crear una función que rellene el array o arreglo con los múltiplos de un número pedido por teclado.
//Por ejemplo, si defino un array de tamaño 5 y elijo un 3 en la función, el array contendrá 3,
//6, 9, 12, 15. Mostrarlos por pantalla usando otra función distinta.

System.out.println("Ejercico 10 :");

 int[] v;
 int tamanio, multiplo;
 Multiple m = new Multiple();
 int var;
 
 System.out.println("Ingrese tamaño del vector(numero entero) :");
  tamanio = read.nextInt();
  v= new int[tamanio]; 
 System.out.println("Ingrese el numero del cual quiere ser multiplo  :");
  multiplo = read.nextInt();
 
 System.out.println("multiplo de "+ multiplo +"son ");
 for(int n = 2; (n-1)<= v.length;n++){  
     
    var = m.multipls(n,multiplo);
  
   System.out.println(var);     
 
 }

//11.Programa Java que lea 10 números enteros por teclado y los guarde en un array.
//Calcula y muestra la media de los números que estén en las posiciones pares del array.
 
 System.out.println("Ejercico 11 :");
 System.out.println("Ingrese 10 numeros, positivos y negativos: ");
 int[] vector = new int[10];
 int numer;
 int cont=0;
 do{  
  numer = read.nextInt(); 
  vector[cont]= numer;
  cont ++;
  
 }while (cont < 10);
 
 for(int i = 0; i<vector.length; i++){      
   if (i%2==0){System.out.println("Posicion par "+i+" :"+ vector[i]); }
     
 }
 //12. ​ Programa Java que lea por teclado 10 números enteros y los guarde en un array. A
//continuación calcula y muestra por separado la media de los valores positivos y la de los
//valores negativos.

System.out.println("Ejercico 12 ");
System.out.println("Usando el vector anterior de 10 nros ingresados por teclado.... ");
 String vari;
 Media me = new Media();
 
vari = me.CalcularMedia(vector);        
System.out.println(vari);

//13. ​ Lee un número por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es,
//también debemos indicarlo.
System.out.println("Ejercico 13 ");
System.out.println("Ingrese un numero :");

numero = 0;
numero = read.nextInt();

if (numero%2 == 0){
 System.out.println("Es divisible en 2");
}else{
 System.out.println("No es divisible en 2");
}

//14.Lee un número por teclado y muestra por consola, el carácter al que pertenece en la
//tabla ASCII. Por ejemplo: si introduzco un 97, me muestre una a
numero = 0;

System.out.println("Ingrese nros positivos de la tabla ascii para su conversion :");
numero = read.nextInt();

do{ 
    char c = (char)numero;
    System.out.println(c); 
    System.out.println("Ingrese otro nro de la tabla ascci o presione 0 para salir :");
    numero = read.nextInt();
    
} while(numero > 0);


//15.​ Lee un número por teclado que pida el precio de un producto (puede tener decimales) y
//calcule el precio con IVA. El IVA será una constante que será del 21%.

float precio, precioconiva;
System.out.println("Ingrese un precio (pejem 5,4) : ");
precio = (float)read.nextFloat();

precioconiva = (float) (precio*1.21);

System.out.println(precioconiva);

//16.​ Realiza una aplicación que nos pida un número de ventas a introducir, después nos
//pedirá tantas ventas por teclado como número de ventas se hayan indicado. Al final
//mostrará la suma de todas las ventas. Piensa que es lo que se repite y lo que no



    }
    
}
