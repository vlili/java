
package ejercicios;

import java.util.Arrays;
/**
 *
 * @author anonimo
 */
class Media {
  
public Media(){
    
}    
public String CalcularMedia(int[] vec){
  
 int[] vecpositivo, vecnegativo;
 int suma,countpos,sumas,countneg;   
 float resultado = 0,resultados = 0;
 
   vecpositivo = Arrays.stream(vec).filter(x-> x>0).toArray();
   vecnegativo = Arrays.stream(vec).filter(x-> x<0).toArray();
   suma = Arrays.stream(vecpositivo).sum();
   sumas = Arrays.stream(vecnegativo).sum();
   countpos= (int) Arrays.stream(vecpositivo).count();
   countneg = (int) Arrays.stream(vecnegativo).count();
   resultado=(float)suma/countpos;
   resultados= (float)sumas/countneg;  

  return("La media positivos "+ resultado + "\n La media de negativos " + resultados); 
 }
        
}
