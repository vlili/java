
package practico1;
/**
 * @author anonimo
 */
public class Cuadrado {   

private Punto p1,p2,p3,p4;    
 /**
 * Constructor
 **/        
public Cuadrado(){
    this.p1=new Punto(0,0);
    this.p2=new Punto(0,0);
    this.p3=new Punto(0,0);
    this.p4=new Punto(0,0);
    }  
 /**
 * Constructor recibe 4 puntos
 * @param x1  es un punto
 * @param x2  es un punto
 * @param x3  es un punto
 * @param x4  es un punto
 **/ 
public Cuadrado(Punto x1,Punto x2,Punto x3,Punto x4){ 
this.p1= new Punto(x1.getX(),x1.getY());
this.p2= new Punto(x2.getX(),x2.getY());  
this.p3= new Punto(x3.getX(),x3.getY()); 
this.p4= new Punto(x4.getX(),x4.getY()); 
 }   
 /**
 * Constructor recibe 8 enteros
 * @param x1  es un entero
 * @param y1  es un entero
 * @param x2  es un entero
 * @param y2  es un entero
 * @param x3  es un entero
 * @param y3  es un entero
 * @param x4  es un entero
 * @param y4  es un entero
 **/
public Cuadrado(int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){ 
this.p1= new Punto(x1,y1);
this.p1= new Punto(x2,y2);
this.p1= new Punto(x3,y3);
this.p1= new Punto(x4,y4);
}
/**
 * setea parametros
 * @param x entero
 */
public void SetX(int x){
    this.p1.setX(x);    
}
public void SetY(int y){
    this.p1.setY(y);    
}
public void SetX1(int x){
   this.p2.setX(x);    
}
public void SetY1(int y){
   this.p2.setY(y);    
}

public void SetX2(int x){
   this.p3.setX(x);    
}
public void SetY2(int y){
   this.p3.setY(y);    
}
public void SetX3(int x){
   this.p4.setX(x);    
}
public void SetY3(int y){
   this.p4.setY(y);    
}



}
