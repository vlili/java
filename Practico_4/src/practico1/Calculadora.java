package practico1; 
/**
 * @author anonimo
 */
public class Calculadora {    
  NroRacional r1; 
  NroRacional r2;
  float resultado;
 public Calculadora() {
  r1 = new NroRacional(0,0);
  r2 = new NroRacional(0, 0);
  }  
 public Calculadora(NroRacional n1, NroRacional n2) {
   r1 = new NroRacional();
   r2 = new NroRacional(); 
   r1 = n1;
   r2 = n2;
  } 
 public float Suma(){      
     resultado = r1.Racional() + r2.Racional();
     return(resultado);
 }
  public float Resta(){
     resultado = r1.Racional() - r2.Racional();
     return(resultado);
 }
 public float Multiplica(){
     resultado = r1.Racional() * r2.Racional();
     return(resultado);
 }
 public float Divide(){
     resultado = r1.Racional() / r2.Racional();
     return(resultado);
 }
    
}
