package practico1;


import java.util.Scanner;
/**
 *
 * @author anonimo
 */
public class Practico1 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Punto p = new Punto(1,2);
        Punto p1;
        int v1, v2;        
        int valor1, valor2, valor3;
        Circulo c ;
          
        Punto pc;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese el valor de X: ");
        v1 = sc.nextInt();
        
        System.out.println("Ingrese el valor de Y: ");
        v2 = sc.nextInt();
        
        p1 = new Punto(v1,v2);
        
        System.out.println("La distancia entre los 2 puntos es: "+p.calcularDistanciaDesde(p1));
        
        System.out.println("Ingrese radio: ");
        valor1 = sc.nextInt(); 
        
        System.out.println("Ingrese x e y : ");
        valor2 = sc.nextInt();
        valor3 = sc.nextInt();      
         
        c = new Circulo(valor1, valor2, valor3); 
        pc = new Punto(valor2, valor3);        
               
         System.out.println("Calcular area :"+ c.calcularArea());  
         System.out.println("Calcular perimetro :"+ c.calcularPerimetro());  
         System.out.println("Calcular distancia a un punto :"+ c.calcularDistanciaDesde(pc));  
         
        //Actividad 3  copy paste de lo de maxi
        Triangulo triangulo = new Triangulo(1,2,1,5,6,2);
        
        System.out.printf("-------------------Acividad 3-------------------\n"
                + "\nEl area del triangulo es: %.2f\n",triangulo.calcularArea());
        
        System.out.printf("El perimetro del triangulo es: %.2f\n",triangulo.calcularPerimetro());
        
        Punto p3 = new Punto(2,4);
        System.out.printf("La distancia entre el centro del triangulo y el punto 3 es: %.2f\n", triangulo.calcularDistancia(p3));
        
        //Actividad laboratorio 3
        System.out.println("Calculaladora");
        
        NroRacional n1 = new NroRacional(13,4); 
        NroRacional n2 = new NroRacional(17,8);  
        System.out.println("Primer racional :"+n1.toString());
        System.out.println("Segundo racional :"+n2.toString());
        Calculadora ca;
        ca = new Calculadora(n1,n2);
        
        System.out.println("Suma :"+ ca.Suma());
        System.out.println("Resta :"+ ca.Resta());
        System.out.println("Multiplicacion :"+ ca.Multiplica());
        System.out.println("Division :"+ ca.Divide());
        
    }
    
}
