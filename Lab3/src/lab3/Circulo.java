package lab3;
/**
 *
 * @author anonimo
 */
public class Circulo extends Punto{
 private Punto centro;
 private int  radio;
 double resultado;
 /**
 * Constructor
 **/
 public Circulo() { 
 centro = new Punto();
 this.radio = 0;
 }
 /**
  * @param param1 punto
  *  @param param2 entero
 * Constructor, recibe punto y radio
 **/
 public Circulo(Punto param1,int param2){ 
    this.centro = new Punto(param1.getX(), param1.getY());
    this.radio = param2; 
 }
 /**
  * @param param1 entero
  * @param param2 entero
  * @param param3 entero
 * Constructor, recibe 3 parametros
 **/
  public Circulo(int param1,int param2,int param3){ 
      this.centro = new Punto(param1, param2);
      this.radio = param3;
  } 
 /**
 * muestra centro
 * @return  centro
 **/
   public Punto getCentro(){ 
       centro = new Punto(0,0);
       return this.centro;
   } 
 /**
 * muestra radio
 * @return  radio
 **/
   public double getRadio(){ return this.radio; } 
    
 /**
 * re utiliza metodo calcularDistanciaDesde
 * @param param1 punto
 * @return  resultado
 **/
   @Override
   public double calcularDistanciaDesde(Punto param1){  
       
      this.resultado = Math.sqrt(Math.pow(param1.getX(), 2)+ Math.pow(param1.getY(),2))- this.radio;
      return (resultado);    
   } 
 /**
 * Calcula perimetro 
 * @return  resultado
 **/
   public double calcularPerimetro(){   
    resultado= 2 * Math.PI * this.radio;
     return(resultado);    
   } 
 /**
 * Calcula are circulo
 * @return  resultado
 **/
   public double calcularArea(){
    resultado = Math.pow(this.radio, 2) * Math.PI;
    return(resultado);
   }
     
}
