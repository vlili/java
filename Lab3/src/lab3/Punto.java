package lab3;
/**
 * @author anonimo
 **/
public class Punto {

    private int x;
    private int y;
/**
 * Constructor
 **/
    public Punto() {

    }
/**
 * @param x entrada 
 * @param y entrada
 * Constructor, recibe dos puntos
 **/
    public Punto(int x, int y) {
        this.x = x;
        this.y = y;
    }
/**
 * Muestra  punto x
 * @return  x
 **/
    public int getX() {
        return this.x;
    }
/**
 * @param x
 *  setea X
 **/
    public void setX(int x) {
        this.x = x;
    }
/**
 *  Muestra Y
 *  @return y
 **/
    public int getY() {
        return this.y;
    }
/**
 * @param y
 *  setea Y
 **/
    
    public void setY(int y) {
        this.y = y;
    }
/**
 * @param p punto
 *  Utiliza metodo calkcularDistancia Desde para punto
 * @return double
 **/
    // (1,6) - P (4,7)
    public double calcularDistanciaDesde(Punto p) {
        double distancia = 0;
        double suma = Math.pow(p.getX() - this.x, 2) + Math.pow(p.getY() - this.y, 2);
        distancia = Math.sqrt(suma);
        return distancia;
    }
}
