
package ejerc2;

import java.util.Scanner;
/**
 *
 * @author nb_hp
 */
public class Ejerc2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Ejecicio 16 :");
        
     Scanner read = new Scanner(System.in);
        System.out.println("Ingresar cantidad de ventas");
        int v=read.nextInt();
        float suma=0;
        float precio=0;
        for (int i=0;i<v;i++){
            
            System.out.println("precio de la venta "+(i+1));
             precio=read.nextFloat();  
            
            suma=suma+precio;
        }
  
        System.out.println("Suma "+suma);   
        
        
 //17 Realiza una aplicación que nos calcule una ecuación de segundo grado. Debes pedir
//las variables a, b y c por teclado y comprobar antes que el discriminante (operación en la
//raíz cuadrada). Para la raíz cuadrada usa el método sqlrt de Math. Te recomiendo que uses
//mensajes de traza.
System.out.println("Ejecicio 17 :");
  double dis,res,res1;
        System.out.println("Ingrese valor a para calculo del discriminante :");
        int a=read.nextInt();
        System.out.println("Ingrese valor  b");
        int b=read.nextInt();
        System.out.println("Ingrese valor  c");
        int c=read.nextInt();
  
        
         //res = Math.pow(b, 2);
        // res1=4*a*c;
          //System.out.println(res +" "+res1);
         dis=  (float) (Math.pow(b, 2)-(4*a*c));  
        
        System.out.println(">>"+dis);
  
        if (dis>0){
      
            double x1=((b*(-1))+Math.sqrt(dis))/(2*a);
            double x2=((b*(-1))-Math.sqrt(dis))/(2*a);
  
            System.out.println("X1: "+x1+"X2: "+x2);
        }else{
            System.out.println("El discriminante es negativo");
        }

//18. Lee un número por teclado y comprueba que este número es mayor o igual que cero,
//si no lo es lo volverá a pedir (do while), después muestra ese número por consola.
      System.out.println("Ejercicio 18 :");
      int num;
      do{
      System.out.println("Ingrese un  entero  :");
       num = read.nextInt();
     
          
      } while( num >= 0);
      
      
//19. Escribe una aplicación con un String que contenga una contraseña cualquiera.
//Después se te pedirá que introduzcas la contraseña, con 3 intentos. Cuando aciertes ya
//no pedirá mas la contraseña y mostrara un mensaje diciendo “Enhorabuena”. Piensa bien
//en la condición de salida (3 intentos y si acierta sale, aunque le queden intentos).
      System.out.println("Ejercicio 19 :");
  String contrasenia="pass";
  String password;
  int bandera=0;
  int contador=0;
  
  while (bandera != 1 ){
   System.out.println("Ingrese contrasenia :"); 
   password = read.nextLine(); 
   
   if(contador < 4){
       contador++;
      // System.out.println(contador +" intento");  
   if( password.equals(contrasenia)){
      System.out.println("En hora buena"); 
      bandera =1;
  } else {
   System.out.println("Contrasenia incorrecta");
   }     
   
  } else{
      System.out.println("Tuvo mas de  3 intentos fallidos "); 
      bandera =1;
   }  
   
  }  
  
  
  
//20. Pide por teclado dos número y genera 10 números aleatorios entre esos números.
//Usa el método Math.random para generar un número entero aleatorio (recuerda el
//casting de double a int).
System.out.println("Ejercicio 20 :");

System.out.println("Ingreses dos numeros :");
int num1 = read.nextInt();
int num2 = read.nextInt();

for(int i=0;i< 10;i++){
 System.out.println((int)(Math.random()*(num2-num1+1)+num1));
} 



    }
    
}
